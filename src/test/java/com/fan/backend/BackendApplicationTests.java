package com.fan.backend;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fan.backend.entity.Admin;
import com.fan.backend.mapper.AdminMapper;
import com.fan.backend.service.AdminService;
import com.fan.backend.service.impl.AdminServiceImpl;
import com.sun.org.apache.xpath.internal.operations.Bool;
import lombok.val;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
class BackendApplicationTests {

    @Autowired
    private AdminServiceImpl adminService;

    @Test
    void contextLoads() {
        //查询全部用户
        List<Admin> list = adminService.list();
        list.forEach(System.out::println);
    }

    @Test
    public void testInsert(){//插入测试
        Admin admin = new Admin();
        admin.setName("admin5").setPassword("123456");
        Boolean res = adminService.save(admin);
        System.out.println(admin);//id 会自动回填
    }

    @Test
    public void testUpdate(){
        Admin admin = adminService.getById(4);
        admin.setName("libai");
        Boolean res = adminService.updateById(admin);
        System.out.println(admin);
    }

    @Test
    public void testSelect(){
        HashMap map = new HashMap<>();
        map.put("name", "dufu");

        List<Admin> list = adminService.listByMap(map);
        list.forEach(System.out::println);
    }

    @Test
    public void page(){
        Page<Admin> page = new Page<>(2, 2);
        adminService.page(page,null);
        page.getRecords().forEach(System.out::println);

    }


}
