package com.fan.backend.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
public class Result<T> implements Serializable {
    /**
     * 数据
     */
    private T data;
    /**
     * 响应时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date time = new Date();

    private boolean res;

    public Result() {

    }
    public static Result ok(boolean res, Object data) {
        return Result.builder()
                .res(res)
                .data(data)
                .time(new Date())
                .build();
    }
}
