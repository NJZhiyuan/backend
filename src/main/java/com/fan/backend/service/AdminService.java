package com.fan.backend.service;

import com.fan.backend.entity.Admin;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author NJ-Y
 * @since 2021-12-19
 */
public interface AdminService extends IService<Admin> {

}
