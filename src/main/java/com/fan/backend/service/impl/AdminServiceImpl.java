package com.fan.backend.service.impl;

import com.fan.backend.entity.Admin;
import com.fan.backend.mapper.AdminMapper;
import com.fan.backend.service.AdminService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author NJ-Y
 * @since 2021-12-19
 */
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements AdminService {

}
