package com.fan.backend.mapper;

import com.fan.backend.entity.Admin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author NJ-Y
 * @since 2021-12-19
 */
public interface AdminMapper extends BaseMapper<Admin> {

}
