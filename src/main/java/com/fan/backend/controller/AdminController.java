package com.fan.backend.controller;


import com.baomidou.mybatisplus.extension.api.R;
import com.fan.backend.entity.Admin;
import com.fan.backend.service.AdminService;
import com.fan.backend.service.impl.AdminServiceImpl;
import com.fan.backend.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author NJ-Y
 * @since 2021-12-19
 */
@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @GetMapping("getAdminById")
    public Result getAdminById(@RequestParam int id){
        Result result = new Result();
        Admin admin = adminService.getById(id);
        return result.ok(true,admin);
    }

    @RequestMapping("getAdmin")
    public Result getAdmin(){
        Result result = new Result();
        List list = adminService.list();
        return result.ok(true,list);
    }

//    @RequestMapping("getAdmin")
//    public Result getAdmin(){
//        Result result = new Result();
//        List<Admin> list = adminService.list();
//        return result.ok(true,list);
//    }

}

